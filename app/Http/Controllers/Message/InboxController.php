<?php

namespace App\Http\Controllers\Message;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Inbox;
use App\Models\Outbox;
use App\Models\Pbk;
use App\Http\Requests\ReplyRequest;

class InboxController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = "Kotak Masuk";
        $data = Inbox::orderBy('ID', 'DESC')->get();
        return view('pesan.kotak_masuk.index', compact('data', 'title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $title = "Balas Pesan";
        $data = Inbox::find($id);
        $data->Processed = 'true';
        $data->save();
        return view('pesan.kotak_masuk.reply', compact('data', 'id', 'title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(ReplyRequest $request, $id)
    {
        $pbk = Pbk::where('Number', $request->kontak)->first();
        $pbk_id = -1;
        $pbk_number = $request->kontak;
        if($pbk){
            $pbk_id = $pbk->Number;
            $pbk_number = $pbk->Number;
        }
        $slit_pesan = str_split($request->pesan, 153);
        $total_pesan = count($slit_pesan);
        if($total_pesan == 1){
            $outbox = Outbox::create([
                'DestinationNumber' => $pbk_number,
                'CreatorID'         => 'gammu',
                'TextDecoded'       => $slit_pesan[0]
            ]);
        }
        elseif($total_pesan >= 2){
            $outbox = Outbox::create([
                'UDH'               => '050003A7'.$total_pesan.'01',
                'DestinationNumber' => $pbk_number,
                'TextDecoded'       => $slit_pesan[0],
                'CreatorID'         => 'gammu',
                'MultiPart'         => 'true'
            ]);
            foreach($slit_pesan as $key => $var){
                if($key != 0){
                    $multipart = OutboxMultipart::create([
                        'UDH'               => '050003A7'.$total_pesan.sprintf("%02d", $key+1),
                        'ID'                => $outbox->ID,
                        'TextDecoded'       => $var,
                        'SequencePosition'  => $key+1
                    ]);
                }
            }
        }
        return redirect()->route('outbox.index')->with(['success' => 'Berhasil Membalas Pesan']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = OutBox::find($id)->delete();
        return redirect()->back()->with(['danger' => 'Berhasil Menghapus Pesan']);
    }
    public function checkInbox(){
        $data = Inbox::where('Processed', 'false')->get()->toArray();
        return response()->json([$data]);
    }
}
