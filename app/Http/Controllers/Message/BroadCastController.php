<?php

namespace App\Http\Controllers\Message;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Outbox;
use App\Models\OutboxMultipart;
use App\Models\Pbk;
use App\Models\PbkGroup;
use App\Http\Requests\BroadCastRequest;

class BroadCastController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = "Tulis Pesan Siaran Baru";
        $kontak = PbkGroup::get();
        return view('pesan.pesan_siaran.create', compact('kontak', 'title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BroadCastRequest $request)
    {
        $group = Pbk::where('GroupID', $request->kontak)->get();
        $slit_pesan = str_split($request->pesan, 153);
        $total_pesan = count($slit_pesan);
        foreach ($group as $key => $pbk) {
            if($total_pesan == 1){
                $outbox = Outbox::create([
                    'DestinationNumber' => $pbk->Number,
                    'CreatorID'         => 'gammu',
                    'TextDecoded'       => $slit_pesan[0]
                ]);
            }elseif($total_pesan >= 2){
                $outbox = Outbox::create([
                    'UDH'               => '050003A7'.$total_pesan.'01',
                    'DestinationNumber' => $pbk->Number,
                    'CreatorID'         => 'gammu',
                    'TextDecoded'       => $slit_pesan[0],
                    'MultiPart'         => 'true'
                ]);
                foreach($slit_pesan as $key => $var){
                    if($key != 0){
                        $multipart = OutboxMultipart::create([
                            'UDH'               => '050003A7'.$total_pesan.sprintf("%02d", $key+1),
                            'ID'                => $outbox->ID,
                            'TextDecoded'       => $var,
                            'SequencePosition'  => $key+1
                        ]);
                    }
                }
            }
        }
        
        return redirect()->route('outbox.index')->with(['success' => 'Berhasil Mengirim Pesan Siaran']);
    }
}
