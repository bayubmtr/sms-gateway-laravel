<?php

namespace App\Http\Controllers\Message;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Outbox;
use App\Models\OutboxMultipart;
use App\Models\Pbk;
use App\Http\Requests\ScheduleRequest;
use Carbon\Carbon;
use DB;

class ScheduleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = "Pesan Terjadwal";
        $data = Outbox::where('type', 'terjadwal')->orderBy('SendingDateTime', 'ASC')->get();
        return view('pesan.pesan_terjadwal.index', compact('data', 'title'));
    }

    public function create()
    {
        $title = "Tulis Pesan Terjadwal Baru";
        $kontak = Pbk::get();
        return view('pesan.pesan_terjadwal.create', compact('kontak', 'title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ScheduleRequest $request)
    {
        $pbk = Pbk::find($request->kontak);
        $jadwal = Carbon::create($request->jadwal);

        $sch = Outbox::create([
            'DestinationNumber' => $pbk->Number,
            'CreatorID'         => 'gammu',
            'TextDecoded'       => $request->pesan,
            'type'              => 'terjadwal',
            'SendingDateTime'   => $jadwal
        ]);

        return redirect()->route('schedule.index')->with(['success' => 'Berhasil Membuat Pesan Terjadwal']);
    }
    public function destroy($id)
    {
        $data = Outbox::find($id)->delete();
        return redirect()->back()->with(['danger' => 'Berhasil Menghapus Pesan Terjadwal']);
    }
}
