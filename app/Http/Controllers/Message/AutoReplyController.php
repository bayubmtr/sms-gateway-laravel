<?php

namespace App\Http\Controllers\Message;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Inbox;
use App\Models\Outbox;
use App\Models\Pbk;
use App\Models\Pesanan;
use App\Http\Requests\ReplyRequest;
use DateTime;

class AutoReplyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Inbox::where('Processed', 'false')->orderBy('ID', 'DESC')->where('type')->get();
        foreach ($data as $key => $value) {
            $pesan_temp = strtoupper($value->TextDecoded);
            $pesan = explode(" ", $pesan_temp);
            //auto reply cek pesanan
            if($pesan[0] == 'CEK'){
                if($pesan[1] == 'PESANAN'){
                    if($pesan[2]){
                        $kom = Inbox::where('ID', $value->ID)->update(['type' => 'cek-pesanan']);
                        $sablon = Pesanan::where('id', $pesan[2])->first();
                        if(!$sablon){
                            $reply = 'Nomor Pesanan Tidak Terdaftar';
                        }elseif($sablon->status == 'diproses'){
                            $reply = 'Pesanan Anda Sedang Dalam Proses. Silahkan Ambil Pada Tanggal '. date_format(new DateTime($sablon->tanggal_selesai),"d-m-Y");
                        }elseif($sablon->status == 'selesai'){
                            $reply = 'Pesanan Anda Sudah Jadi. Mohon Untuk Segera Diambil';
                        }
                    }else{
                        $reply = "Maaf perintah kurang lengkap";
                    }
                }else{
                    $reply = "Maaf perintah kurang lengkap";
                }
                $value->Processed = 'true';
                $value->save();
            }elseif($pesan[0] == 'REG'){
                if($pesan[1] == 'RS'){
                    if($pesan[2]){
                        $kom = Inbox::where('ID', $value->ID)->update(['type' => 'registrasi']);
                        $reg = Pbk::firstOrcreate(
                            ['Number'    => $value->SenderNumber],
                            ['GroupID'   => 0, 'Name'      => $pesan[2]]
                        );
                        $reply = 'Selamat ! Berhasil Mendaftar di Rumah Sablon';
                    }else{
                        $reply = "Maaf perintah kurang lengkap";
                    }
                }else{
                    $reply = "Maaf perintah kurang lengkap";
                }
                $value->Processed = 'true';
                $value->save();
            }elseif($pesan[0] == 'PENGADUAN'){
                if($pesan[1]){
                    $kom = Inbox::where('ID', $value->ID)->update(['type' => 'pengaduan']);
                    $reply = 'Pengaduan Anda Berhasil Disimpan. Mohon Menunggu Balasan Dari Admin';
                }else{
                    $reply = "Maaf perintah kurang lengkap";
                }
                $value->Processed = 'true';
                $value->save();
            }
            if(isset($reply)){
                $this->reply($value->SenderNumber, $reply);
            }
        }
        return $data;
    }
    private function reply($number, $message){
        $outbox = Outbox::create([
            'DestinationNumber' => $number,
            'CreatorID'         => 'gammu',
            'TextDecoded'       => $message
        ]);
    }
}
