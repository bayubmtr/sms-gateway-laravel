<?php

namespace App\Http\Controllers\Message;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Outbox;
use App\Models\OutboxMultipart;
use App\Models\Pbk;
use App\Http\Requests\OutboxRequest;

class OutboxController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = "Kotak Keluar";
        $data = Outbox::orderBy('ID', 'DESC')->get();
        return view('pesan.kotak_keluar.index', compact('data', 'title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = "Tulis Pesan Baru";
        $kontak = Pbk::get();
        return view('pesan.kotak_keluar.create', compact('kontak', 'title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(OutboxRequest $request)
    {
        $pbk = Pbk::find($request->kontak);
        $slit_pesan = str_split($request->pesan, 153);
        $total_pesan = count($slit_pesan);
        if($total_pesan == 1){
            $outbox = Outbox::create([
                'DestinationNumber' => $pbk->Number,
                'TextDecoded'       => $slit_pesan[0],
                'CreatorID'         => 'gammu',
            ]);
        }
        elseif($total_pesan >= 2){
            $outbox = Outbox::create([
                'UDH'               => '050003A7'.$total_pesan.'01',
                'DestinationNumber' => $pbk->Number,
                'TextDecoded'       => $slit_pesan[0],
                'CreatorID'         => 'gammu',
                'MultiPart'         => 'true'
            ]);
            foreach($slit_pesan as $key => $var){
                if($key != 0){
                    $multipart = OutboxMultipart::create([
                        'UDH'               => '050003A7'.$total_pesan.sprintf("%02d", $key+1),
                        'ID'                => $outbox->ID,
                        'TextDecoded'       => $var,
                        'SequencePosition'  => $key+1
                    ]);
                }
            }
        }
        return redirect()->route('outbox.index')->with(['success' => 'Berhasil Mengirim Pesan']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = OutBox::findOrFail($id)->delete();
        return redirect()->back()->with(['danger' => 'Berhasil Menghapus Pesan']);
    }
    private function splitPesan($str){
        return str_split($str, 153);
    }
}
