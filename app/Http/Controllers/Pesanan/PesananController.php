<?php

namespace App\Http\Controllers\Pesanan;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Pesanan;
use App\Models\Jasa;
use App\Models\Bahan;
use App\Models\Pbk;
use App\Models\Outbox;
use App\Http\Requests\PesananRequest;
use Carbon\Carbon;
use PDF;

class PesananController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $status = $request->status;
        if($status == 'diproses'){
            $title = "Pesanan Diproses";
            $data = Pesanan::orderBy('id', 'DESC')->where('status', 'diproses')->get();
        }elseif($status == 'selesai'){
            $title = "Pesanan Selesai";
            $data = Pesanan::orderBy('id', 'DESC')->where('status', 'selesai')->get();
        }
        if($title){
            return view('pesanan.index', compact('data', 'status', 'title'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = "Tambah Pesanan";
        $kontak = Pbk::get();
        $jasa = Jasa::get();
        $bahan = Bahan::get();
        return view('pesanan.create', compact('kontak', 'jasa', 'bahan', 'title'));
    }
    public function edit($id)
    {
        $title = "Edit Pesanan";
        $data = Pesanan::find($id);
        $kontak = Pbk::get();
        $jasa = Jasa::get();
        $bahan = Bahan::get();
        return view('pesanan.edit', compact('data', 'kontak', 'jasa', 'bahan', 'title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PesananRequest $request)
    {
        $biaya_jasa = Jasa::find($request->id_jasa)->first()->biaya;
        $harga_bahan = Bahan::find($request->id_bahan)->first()->harga;
        $pesanan = Pesanan::create([
            'id_user'           => $request->kontak,
            'status'            => 'diproses',
            'tanggal_masuk'     => now(),
            'tanggal_selesai'   => Carbon::create($request->tanggal_selesai),
            'id_jasa'           => $request->id_jasa,
            'biaya_jasa'        => $biaya_jasa,
            'id_bahan'          => $request->id_bahan,
            'harga_bahan'       => $harga_bahan,
            'jumlah'            => $request->jumlah,
            'total'             => ($biaya_jasa + $harga_bahan) * $request->jumlah
        ]);
        return redirect()->route('pesanan.index',['status' => 'diproses'])->with(['success' => 'Berhasil Menambah Pesanan']);
    }
    public function update(PesananRequest $request, $id)
    {
        if($request->type == 'ubah_status'){
            $pesanan = Pesanan::where('id', $id)->first();
            $pesanan->status = $request->status;
            $pesanan->save();
            $pbk = Pbk::where('ID', $pesanan->id_user)->first();

            $notif = Outbox::create([
                'DestinationNumber' => $pbk->Number,
                'TextDecoded'       => 'Pesanan Anda Sudah Selesai. Mohon Untuk Segera Di Ambil',
                'CreatorID'         => 'gammu'
            ]);
        }else{
            $biaya_jasa = Jasa::find($request->id_jasa)->first()->biaya;
            $harga_bahan = Bahan::find($request->id_bahan)->first()->harga;
            $pesanan = Pesanan::where('id', $id)->update([
                'status'            => 'diproses',
                'tanggal_selesai'   => Carbon::create($request->tanggal_selesai),
                'id_jasa'           => $request->id_jasa,
                'biaya_jasa'        => $biaya_jasa,
                'id_bahan'          => $request->id_bahan,
                'harga_bahan'       => $harga_bahan,
                'jumlah'            => $request->jumlah,
                'total'             => ($biaya_jasa + $harga_bahan) * $request->jumlah
            ]);
        }
        return redirect()->route('pesanan.index',['status' => 'diproses'])->with(['success' => 'Berhasil Mengubah Pesanan']);
    }

    public function show(){

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Pesanan::find($id)->delete();
        return redirect()->back()->with(['danger' => 'Berhasil Menghapus Pesanan']);
    }
    public function export(Request $request){
        $status = $request->status;
        $data = Pesanan::where('status', $status)->get();
        $name = 'pesanan_'.$status.'.pdf';
        $pdf = PDF::loadView('pesanan.export', compact('data', 'name'));
        $pdf->setOptions(['isPhpEnabled' => true]);
        $pdf->setPaper('a4', 'landscape');
        return $pdf->stream($name);
    }
}