<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Inbox;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data = [];
        $year = date("Y");
        for($x = 1; $x <= 12; $x++){
            $data[$x] = Inbox::whereYear('ReceivingDateTime', $year)->whereMonth('ReceivingDateTime', $x)->count();
        }
        return view('index', compact('data'));
    }
}
