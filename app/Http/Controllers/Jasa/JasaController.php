<?php

namespace App\Http\Controllers\Jasa;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Jasa;

class JasaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $title = "Data Jasa";
        $data = Jasa::get();
        return view('jasa.index', compact('data', 'title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = "Tambah Jasa";
        return view('jasa.create', compact('title'));
    }
    public function edit($id)
    {
        $title = "Edit Jasa";
        $data =Jasa::find($id);
        return view('jasa.edit', compact('data', 'title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $Jasa = Jasa::create($request->all());
        return redirect()->route('jasa.index')->with(['success' => 'Berhasil Menambah Jasa']);
    }
    public function update(Request $request, $id)
    {
        $Jasa = Jasa::where('ID', $id)->update($request->except('_token', '_method'));
        return redirect()->route('jasa.index')->with(['success' => 'Berhasil Mengubah Jasa']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Jasa::find($id)->delete();
        return redirect()->back()->with(['danger' => 'Berhasil Menghapus Jasa Pelanggan']);
    }
}