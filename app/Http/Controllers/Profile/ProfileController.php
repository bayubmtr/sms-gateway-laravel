<?php

namespace App\Http\Controllers\Profile;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;

class ProfileController extends Controller
{
    public function index()
    {
        $title = "Profil";
        $data = auth()->user();
        return view('profile.index', compact('data', 'title'));
    }
    public function store(Request $request)
    {
        $data = auth()->user();
        $data->name = $request->name;
        $data->email = $request->email;
        $data->password = bcrypt($request->password);
        $data->save();
        return redirect()->route('profile.index')->with(['success' => 'Berhasil Mengubah Profil']);
    }
}
