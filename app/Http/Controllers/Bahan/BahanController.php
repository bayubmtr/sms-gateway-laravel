<?php

namespace App\Http\Controllers\Bahan;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Bahan;

class BahanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $title = "Data Bahan";
        $data = Bahan::get();
        return view('bahan.index', compact('data', 'title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = "Tambah Bahan";
        return view('bahan.create', compact('title'));
    }
    public function edit($id)
    {
        $title = "Edit Bahan";
        $data =Bahan::find($id);
        return view('bahan.edit', compact('data', 'title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $Bahan = Bahan::create($request->all());
        return redirect()->route('bahan.index')->with(['success' => 'Berhasil Menambah Bahan']);
    }
    public function update(Request $request, $id)
    {
        $Bahan = Bahan::where('ID', $id)->update($request->except('_token', '_method'));
        return redirect()->route('bahan.index')->with(['success' => 'Berhasil Mengubah Bahan']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Bahan::find($id)->delete();
        return redirect()->back()->with(['danger' => 'Berhasil Menghapus Bahan Pelanggan']);
    }
}