<?php

namespace App\Http\Controllers\Contact;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Pbk;
use App\Models\PbkGroup;

class GroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $title = "Data Grup";
        $data = PbkGroup::get();
        return view('kontak.grup.index', compact('data', 'title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = "Tambah Grup";
        return view('kontak.grup.create', compact('title'));
    }
    public function edit($id)
    {
        $title = "Edit Grup";
        $data = PbkGroup::find($id);
        return view('kontak.grup.edit', compact('data', 'title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $kontak = PbkGroup::create($request->all());
        return redirect()->route('group.index')->with(['success' => 'Berhasil Menambah Grup']);
    }
    public function update(Request $request, $id)
    {
        $kontak = PbkGroup::where('GroupID', $id)->update($request->except('_token', '_method'));
        return redirect()->route('group.index')->with(['success' => 'Berhasil Mengubah Grup']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = PbkGroup::find($id)->delete();
        return redirect()->back()->with(['danger' => 'Berhasil Menghapus Grup']);
    }
}