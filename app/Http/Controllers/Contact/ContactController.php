<?php

namespace App\Http\Controllers\Contact;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Pbk;
use App\Models\PbkGroup;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $title = "Kontak Pelanggan";
        $data = Pbk::get();
        return view('kontak.kontak.index', compact('data', 'title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = "Tambah Kontak Pelanggan";
        $group = PbkGroup::get();
        return view('kontak.kontak.create', compact('group', 'title'));
    }
    public function edit($id)
    {
        $title = "Edit Kontak";
        $data = Pbk::find($id);
        $group = PbkGroup::get();
        return view('kontak.kontak.edit', compact('data', 'group', 'title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $kontak = Pbk::create($request->all());
        return redirect()->route('contact.index')->with(['success' => 'Berhasil Menambah Kontak']);
    }
    public function update(Request $request, $id)
    {
        $kontak = Pbk::where('ID', $id)->update($request->except('_token', '_method'));
        return redirect()->route('contact.index')->with(['success' => 'Berhasil Mengubah Kontak']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Pbk::find($id)->delete();
        return redirect()->back()->with(['danger' => 'Berhasil Menghapus Kontak Pelanggan']);
    }
}