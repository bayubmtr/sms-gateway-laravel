<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PesananRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method()) {
            case 'GET':
            case 'DELETE':
                return [];
            case 'POST':
                return [
                    'kontak'        => 'required|exists:pbk,ID',
                    'id_jasa'       => 'required|string',
                    'id_bahan'      => 'required|string',
                    'jumlah'        => 'required|string',
                    'tanggal_selesai'   => 'required|string'
                ];
            case 'PUT':
                return [
                    'status'       => 'required|string'
                ];
            case 'PATCH':
                return [
                    'id_jasa'       => 'required|string',
                    'id_bahan'      => 'required|string',
                    'jumlah'        => 'required|string',
                    'tanggal_selesai'   => 'required|string'
                ];
            default:break;
        }
    }
}
