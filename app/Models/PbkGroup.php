<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PbkGroup extends Model
{
    protected $fillable = [
        'GroupID', 'NameGroup'
    ];
    public $timestamps = false;
    protected $primaryKey = 'GroupID';

    public function kontak(){
        return $this->hasMany('App\Models\Pbk', 'GroupID', 'GroupID');
    }
}
