<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OutboxMultipart extends Model
{
    protected $fillable = [
        'UDH', 'TextDecoded', 'ID', 'SequencePosition'
    ];
    protected $table = 'outbox_multipart';
    public $timestamps = false;
    protected $primaryKey = 'ID';
    public $incrementing = false;
}
