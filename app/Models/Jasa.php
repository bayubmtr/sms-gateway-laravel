<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Jasa extends Model
{
    protected $fillable = [
        'nama', 'biaya'
    ];
    protected $table = 'tbl_jasa';
    public $timestamps = false;
}
