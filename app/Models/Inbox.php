<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Inbox extends Model
{
    protected $fillable = [
        'SenderNumber', 'type'
    ];
    protected $table = 'inbox';
    protected $primaryKey = 'ID';
    public $timestamps = false;

    public function user(){
        return $this->belongsTo('App\Models\Pbk', 'SenderNumber', 'Number');
    }
}
