<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{
    protected $fillable = [
        'DestinationNumber', 'TextDecoded', 'CreatorID', 'Time', 'nama_event'
    ];
    protected $table = 'tbl_schedule';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function user(){
        return $this->belongsTo('App\Models\Pbk', 'DestinationNumber', 'Number');
    }
}
