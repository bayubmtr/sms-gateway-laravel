<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Sentitem extends Model
{
    protected $primaryKey = 'ID';
    public $timestamps = false;

    public function user(){
        return $this->belongsTo('App\Models\Pbk', 'DestinationNumber', 'Number');
    }
    public function admin(){
        return $this->belongsTo('App\User', 'SenderID', 'ID');
    }
}
