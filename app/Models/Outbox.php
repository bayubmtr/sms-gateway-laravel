<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Outbox extends Model
{
    protected $fillable = [
        'DestinationNumber', 'UDH', 'TextDecoded', 'MultiPart', 'SenderID', 'CreatorID', 'type', 'SendingDateTime'
    ];
    protected $table = 'outbox';
    protected $primaryKey = 'ID';
    public $timestamps = false;

    public function user(){
        return $this->belongsTo('App\Models\Pbk', 'DestinationNumber', 'Number');
    }

    public function sent_item(){
        return $this->hasOne('App\Models\Sentitem', 'ID', 'ID');
    }
}
