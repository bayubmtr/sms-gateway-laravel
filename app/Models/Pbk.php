<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pbk extends Model
{
    protected $fillable = [
        'ID', 'GroupID', 'Name', 'Number'
    ];
    protected $table = 'pbk';
    public $timestamps = false;
    protected $primaryKey = 'ID';

    public function group(){
        return $this->belongsTo('App\Models\PbkGroup', 'GroupID', 'GroupID');
    }
}
