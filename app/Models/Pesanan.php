<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pesanan extends Model
{
    protected $fillable = [
        'ID', 'id_user', 'status', 'tanggal_masuk', 'tanggal_selesai', 'id_jasa', 'biaya_jasa', 'id_bahan', 'harga_bahan', 'jumlah', 'total'
    ];
    protected $table = 'tbl_sablon';
    public $timestamps = false;

    public function pelanggan(){
        return $this->belongsTo('App\Models\Pbk', 'id_user', 'ID');
    }
    public function jasa(){
        return $this->belongsTo('App\Models\Jasa', 'id_jasa', 'id');
    }
    public function bahan(){
        return $this->belongsTo('App\Models\Bahan', 'id_bahan', 'id');
    }
}
