<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Bahan extends Model
{
    protected $fillable = [
        'nama', 'harga'
    ];
    protected $table = 'tbl_bahan';
    public $timestamps = false;
}
