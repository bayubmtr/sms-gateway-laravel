@extends('layouts.sablon')
@section('content')
<link href="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/css/select2.min.css" rel="stylesheet" />
<div class="row">
    <div class="col-6">
        <div class="ibox">
            <div class="ibox-head">
                <div class="ibox-title">
                {{ isset($title) ? $title : '' }}
                </div>
            </div>
            <div class="ibox-body">
            @include('partials._info')
                <form id="form_tulis_pesan" action="{{ route('profile.store') }}" method="post">
                    @csrf
                    <div class="form-group">
                        <label for="">Nama</label>
                        <div class="input-group">
                            <input type="text" class="form-control" name="name" value="{{ $data->name }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="">Email</label>
                        <div class="input-group">
                            <input type="text" class="form-control" name="email" value="{{ $data->email }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="">Password</label>
                        <div class="input-group">
                            <input type="password" class="form-control" name="password" value="{{ $data->password }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-danger" onclick="window.history.go(-1)" type="button"><i class="fa fa-arrow-left"></i>
                            Batal</button>
                        <button class="btn btn-primary" type="submit"><i class="fa fa-floppy-o"></i> Ubah</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
$(document).ready(function() {
    $('.js-example-basic-single').select2();
});
</script>
@endsection