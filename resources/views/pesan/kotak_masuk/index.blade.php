@extends('layouts.sablon')
@section('content')
<div class="ibox">
    <div class="ibox-head">
        <div class="ibox-title">
            {{ $title }}
        </div>
    </div>

    <div class="ibox-body">
        @include('partials._info')
        <table class="table table-striped table-bordered table-hover" id="table" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th width="10">No</th>
                    <th width="150">Pengirim</th>
                    <th width="300">Pesan</th>
                    <th width="100">Tanggal</th>
                    <th width="50">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($data as $d)
                <tr style="{{ $d->Processed == 'false' ? 'background-color : #A3DAEC' : '' }}">
                    <td>{{ $loop->iteration}}</td>
                    <td>{{ $d->user ? $d->user->Name : 'Belum terdaftar' }}
                        <div>{{ $d->SenderNumber }}</div>
                    </td>
                    <td class="message clickable-row" data-href="{{ route('inbox.edit', $d->ID) }}" style="cursor : pointer">
                        <span class="wrap-text">{{ $d->TextDecoded }}</span>
                    </td>
                    <td>{{ date('j M Y H:i', strtotime($d->ReceivingDateTime)) }}</td>
                    <td align="center">
                        <form id="f_{{$d->ID}}" action="{{route('inbox.destroy',$d->ID)}}" method="post" style="display:inline">
                        @method('DELETE')
                        @csrf
                        <button onclick="return confirm('Yakin Ingin Hapus Pesan Ini?')"  class="btn  btn-danger" type="submit" value="Delete"><i class="fa fa-trash-o"></i></button>
                        </form>
                        <a href="{{ route('inbox.edit', $d->ID) }}" class="btn btn-primary"><i class="fa fa-reply"></i></a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
<script>
jQuery(document).ready(function($) {
    $(".clickable-row").click(function() {
        window.location = $(this).data("href");
    });
});
</script>
@endsection