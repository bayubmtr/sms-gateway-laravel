@extends('layouts.sablon')
@section('content')
<link href="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/css/select2.min.css" rel="stylesheet" />
<div class="row">
    <div class="col-6">
        <div class="ibox">
            <div class="ibox-head">
                <div class="ibox-title">
                {{ isset($title) ? $title : '' }}
                </div>
            </div>
            <div class="ibox-body">
            @include('partials._info')
                <form id="form_tulis_pesan" action="{{ route('inbox.update',$id) }}" method="post">
                    @csrf
                    @method('patch')
                    <div class="form-group">
                        <label for="">Kontak</label>
                        <div class="input-group">
                            <input type="text" class="form-control" name="kontak" value="{{ $data->SenderNumber  }}" readOnly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="">Pesan</label>
                        <div class="input-group-icon left">
                            <textarea id="" cols="100" rows="5" readOnly>{{ $data->TextDecoded }}</textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="">Balas</label>
                        <div class="input-group-icon left">
                            <textarea name="pesan" id="" cols="100" rows="5"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-danger" onclick="window.history.go(-1)" type="button"><i class="fa fa-arrow-left"></i>
                            Batal</button>
                        <button class="btn btn-primary" type="submit"><i class="fa fa-paper-plane-o"></i> Balas</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
$(document).ready(function() {
    $('.js-example-basic-single').select2();
});
</script>
@endsection