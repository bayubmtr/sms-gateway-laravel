@extends('layouts.sablon')
@section('content')

<link href="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/css/select2.min.css" rel="stylesheet" />
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.0-alpha14/js/tempusdominus-bootstrap-4.min.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.0-alpha14/css/tempusdominus-bootstrap-4.min.css" />
<div class="row">
    <div class="col-6">
        <div class="ibox">
            <div class="ibox-head">
                <div class="ibox-title">
                {{ isset($title) ? $title : '' }}
                </div>
            </div>
            <div class="ibox-body">
            @include('partials._info')
                <form id="form_tulis_pesan" action="{{ route('schedule.store') }}" method="post">
                    @csrf
                    <div class="form-group">
                        <label for="">Kontak</label>
                        <div class="input-group-icon left">
                            <select id="kontak" class="js-example-basic-single form-control" name="kontak">
                                <option value="0">-- Pilih Kontak --</option>
                                @foreach($kontak as $k)
                                <option {{ old('kontak') == $k->ID ? 'selected' : '' }} value="{{$k->ID}}">{{$k->Name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="">Pesan</label>
                        <div class="input-group-icon left">
                            <textarea name="pesan" id="" cols="100" rows="10"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="">Jadwal</label>
                        <div class="input-group">
                            <input type="text" name="jadwal" class="form-control datetimepicker-input" id="datetimepicker5" data-toggle="datetimepicker" data-target="#datetimepicker5"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-danger" onclick="window.history.go(-1)" type="button"><i class="fa fa-arrow-left"></i>
                            Batal</button>
                        <button class="btn btn-primary" type="submit"><i class="fa fa-paper-plane-o"></i> Kirim</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
$(document).ready(function() {
    $('.js-example-basic-single').select2();
});

$(function () {
    $('#datetimepicker1').datetimepicker();
});
</script>
@endsection