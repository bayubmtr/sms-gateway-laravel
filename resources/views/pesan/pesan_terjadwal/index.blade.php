@extends('layouts.sablon')
@section('content')
<div class="ibox">
    <div class="ibox-head">
        <div class="ibox-title">
            {{ $title }}
        </div>
    </div>

    <div class="ibox-body">
        @include('partials._info')
        <table class="table table-striped table-bordered table-hover" id="table" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th width="10">No</th>
                    <th width="150">Penerima</th>
                    <th>Pesan</th>
                    <th width="150">Tanggal Jadwal</th>
                    <th width="100">status</th>
                    <th width="50">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($data as $d)
                <tr>
                    <td>{{ $loop->iteration}}</td>
                    <td>{{ $d->user ? $d->user->Name : 'Belum terdaftar' }}
                        <div>{{ $d->DestinationNumber }}</div>
                    </td>
                    <td>{{ $d->TextDecoded }}</td>
                    <td>{{ date('j M Y H:i', strtotime($d->SendingDateTime)) }}</td>
                    <td>
                        @if($d->Processed == 'false')
                        <span class="badge badge-success">Terkirim</span>
                        @else
                        <span class="badge badge-warning">Menunggu</span>
                        @endif
                    </td>
                    <td align="center">
                        <form id="f_{{$d->ID}}" action="{{route('schedule.destroy',$d->ID)}}" method="post" style="display:inline">
                        @method('DELETE')
                        @csrf
                        <button onclick="return confirm('Yakin Ingin Hapus Pesan Ini?')"  class="btn  btn-danger" type="submit" value="Delete"><i class="fa fa-trash-o"></i></button>
                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
<script>
$(document).ready(function(){
        $("#table_length").append('<a  href="{{ route('schedule.create') }}"> <button type="button" class="btn btn-outline-primary ml-3">Tambah</button></a>');
    });
</script>
@endsection