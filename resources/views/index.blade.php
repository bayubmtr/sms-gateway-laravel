@extends('layouts.sablon')
@section('content')
<script>
window.onload = function () {

//Better to construct options first and then pass it as a parameter
var options = {
	title: {
		text: "STATISTIK SMS MASUK"              
	},
	data: [              
	{
		// Change type to "doughnut", "line", "splineArea", etc.
		type: "column",
		dataPoints: [
			{ label: "JAN", y: {!! $data[1] !!} },	
			{ label: "FEB", y: {!! $data[2] !!} },	
			{ label: "MAR", y: {!! $data[3] !!} },
			{ label: "APR", y: {!! $data[4] !!} },	
			{ label: "MEI", y: {!! $data[5] !!} },
			{ label: "JUN", y: {!! $data[6] !!} },
			{ label: "JUL", y: {!! $data[7] !!} },
			{ label: "AGU", y: {!! $data[8] !!} },
			{ label: "SEP", y: {!! $data[9] !!} },
			{ label: "OKT", y: {!! $data[10] !!} },
			{ label: "NOV", y: {!! $data[11] !!} },
			{ label: "DES", y: {!! $data[12] !!} }
		]
	}
	]
};

$("#chartContainer").CanvasJSChart(options);
}
</script>
<div id="chartContainer" style="height: 370px; width: 100%;"></div>
<script type="text/javascript" src="https://canvasjs.com/assets/script/jquery.canvasjs.min.js"></script>
@endsection
