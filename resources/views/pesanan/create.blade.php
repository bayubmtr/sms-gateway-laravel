@extends('layouts.sablon')
@section('content')
<link href="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/css/select2.min.css" rel="stylesheet" />
<div class="row">
    <div class="col-6">
        <div class="ibox">
            <div class="ibox-head">
                <div class="ibox-title">
                {{ isset($title) ? $title : '' }}
                </div>
            </div>
            <div class="ibox-body">
            @include('partials._info')
                <form id="form_pesanan" action="{{ route('pesanan.store') }}" method="post">
                    @csrf
                    <div class="form-group">
                        <label for="">Pelanggan</label>
                        <div class="input-group-icon left">
                            <select id="kontak" class="js-example-basic-single form-control" name="kontak">
                                <option value="0">-- Pilih Pelanggan --</option>
                                @foreach($kontak as $k)
                                <option {{ old('kontak') == $k->ID ? 'selected' : '' }} value="{{$k->ID}}">{{$k->Name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="">Jasa</label>
                        <div class="input-group-icon left">
                            <select id="jasa" class="js-example-basic-single form-control" name="id_jasa">
                                <option value="0">-- Pilih Jasa --</option>
                                @foreach($jasa as $k)
                                <option {{ old('id_jasa') == $k->id ? 'selected' : '' }} value="{{$k->id}}">{{$k->nama}} - {{$k->biaya}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="">Bahan</label>
                        <div class="input-group-icon left">
                            <select id="bahan" class="js-example-basic-single form-control" name="id_bahan">
                                <option value="0">-- Pilih Bahan --</option>
                                @foreach($bahan as $k)
                                <option {{ old('id_bahan') == $k->id ? 'selected' : '' }} value="{{$k->id}}">{{$k->nama}} - {{$k->harga}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="">Jumlah Pesanan</label>
                        <div class="input-group">
                            <input type="text" class="form-control" name="jumlah">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="">Tanggal Selesai</label>
                        <div class="input-group">
                            <input type="text" id="jumlah" class="form-control date-picker" name="tanggal_selesai">
                        </div>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-danger" onclick="window.history.go(-1)" type="button"><i class="fa fa-arrow-left"></i>
                            Batal</button>
                        <button class="btn btn-primary" type="submit"><i class="fa fa-floppy-o"></i> Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
$(document).ready(function() {
    $('.js-example-basic-single').select2();
});
</script>
<script type="text/javascript">

$('.date-picker').datepicker({  

   format: 'dd-mm-yyyy'

 });  

</script> 
@endsection