<html>
<head>
    <style>
    .table{
        border:1px solid black;
        border-collapse:collapse;
    }
    .table tr td{
        border:1px solid black;
        padding : 5px;
    } 
    .table tr th{
        border:1px solid black;
        padding : 10px;
    }
    .text-right{
        text-align : right;
    }
    .text-center{
        text-align : center;
    }
    .pt-7{
        padding-top : 70px;
    }
    .pt-2{
        padding-top : 20px;
    }
    @page { margin: 50px; }
    .container { 
        margin-top : 1cm;
        width : 100%;
    }
    footer {
        position: fixed; 
        bottom: 1cm; 
        left: 0cm; 
        right: 0cm;
        height: 2cm;
    }
    .ttd{
        display : block;
        height : 145px;
        page-break-inside: avoid;
        margin-top : 20px
    }
    .content {
        min-height : 10cm;
    }
    .text-uppercase{
        text-transform: uppercase;
    }
    </style>
    <title>{{ $name }}</title>
</head>
<body>
<script type="text/php">
 $text = 'page: {PAGE_NUM} / {PAGE_COUNT}';
 $font = $fontMetrics->get_font("helvetica", "bold");
 $pdf->page_text(36, 18, $text, $font, 9);
</script>
	<style type="text/css">
		table tr td,
		table tr th{
			font-size: 9pt;
        }
        div {font-size: 9pt;}
        table { width:100%; }
        .first, .second, .third { width:30%; }
        .first2, .second2, .third2 { width:25%; }
        .four2 {
            width:15%;
        }
        .third2 {
            text-align : right;
        }
    </style>
    <header>
        <center class="pt-2">
            <hr class="border-dark">
            <span style="font-size : 30px">RUMAH SABLON</span>
            <hr class="border-dark">
        </center>
    </header>
	<div class="container text-uppercase">
        <table  class="table mt-3 content">
            <thead>
                <tr>
                    <th>NO.</th>
                    <th>NAMA PELANGGAN</th>
                    <th>NO HP</th>
                    <th>JASA</th>
                    <th>BIAYA JASA</th>
                    <th>BAHAN</th>
                    <th>HARGA BAHAN</th>
                    <th>QTY</th>
                    <th>TOTAL</th>
                    <th>TGL MASUK</th>
                    <th>TGL SELESAI</th>
                </tr>
            </thead>
            <tbody>
                @foreach($data as $d)
                <tr>
                    <td>{{ $loop->iteration}}</td>
                    <td>{{ $d->pelanggan->Name }}</td>
                    <td>{{ $d->pelanggan->Number }}</td>
                    <td>{{ $d->jasa->nama }}</td>
                    <td>{{ $d->biaya_jasa }}</td>
                    <td>{{ $d->bahan->nama }}</td>
                    <td>{{ $d->harga_bahan }}</td>
                    <td>{{ $d->jumlah }}</td>
                    <td>{{ $d->total }}</td>
                    <td>{{ date('j M Y H:m', strtotime($d->tanggal_masuk)) }}</td>
                    <td>{{ date('j M Y H:m', strtotime($d->tanggal_selesai)) }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</body>
</html>