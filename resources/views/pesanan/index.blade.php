@extends('layouts.sablon')
@section('content')
<div class="ibox">
    <div class="ibox-head">
        <div class="ibox-title">
            {{ $title }}
        </div>
    </div>

    <div class="ibox-body">
        @include('partials._info')
        <table class="table table-striped table-bordered table-hover" id="table" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th width="10">No</th>
                    <th>Nama Pelanggan</th>
                    <th>No HP</th>
                    <th>Jasa</th>
                    <th>Biaya Jasa</th>
                    <th>Bahan</th>
                    <th>Harga Bahan</th>
                    <th>Qty</th>
                    <th>Total</th>
                    <th>Tgl Masuk</th>
                    <th>Tgl Selesai</th>
                    <th width="120">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($data as $d)
                <tr>
                    <td>{{ $loop->iteration}}</td>
                    <td>{{ $d->pelanggan->Name }}</td>
                    <td>{{ $d->pelanggan->Number }}</td>
                    <td>{{ $d->jasa->nama }}</td>
                    <td>{{ $d->biaya_jasa }}</td>
                    <td>{{ $d->bahan->nama }}</td>
                    <td>{{ $d->harga_bahan }}</td>
                    <td>{{ $d->jumlah }}</td>
                    <td>{{ $d->total }}</td>
                    <td>{{ date('j M Y', strtotime($d->tanggal_masuk)) }}</td>
                    <td>{{ date('j M Y', strtotime($d->tanggal_selesai)) }}</td>
                    <td align="center">
                        <form id="f_{{$d->id}}" action="{{route('pesanan.destroy',$d->id)}}" method="post" style="display:inline">
                        @method('DELETE')
                        @csrf
                        <button onclick="return confirm('Yakin Ingin Hapus Pesanan Ini?')"  class="btn  btn-danger" type="submit" value="Delete"><i class="fa fa-trash-o"></i></button>
                        </form>
                        <a href="{{ route('pesanan.edit', $d->id) }}" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
                        @if($d->status == 'diproses')
                        <form id="fw_{{$d->id}}" action="{{route('pesanan.update',$d->id)}}" method="post" style="display:inline">
                        @method('PUT')
                        @csrf
                        <input type="hidden" name="status" value="selesai">
                        <input type="hidden" name="type" value="ubah_status">
                        <button onclick="return confirm('Ubah pesanan ini menjadi selesai ?')"  class="btn  btn-success" type="submit" value="Selesai"><i class="fa fa-check"></i></button>
                        </form>
                        @endif
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
<script>
$(document).ready(function(){
        $("#table_length").append('<a  href="{{ route('pesanan.export',['status' => $status]) }}"> <button type="button" class="btn btn-outline-primary ml-3">Export</button></a>');
    });
</script>
@endsection