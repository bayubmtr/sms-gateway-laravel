<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width initial-scale=1.0">
    <title>Rumah Sablon</title>
    @include('partials._css')
    @include('partials._js')
</head>

<body class="fixed-navbar">
    <div class="page-wrapper">
        <!-- START HEADER-->
        <header class="header">
            <div class="page-brand">
                <a class="link" href="index.html">
                    <span class="brand">RUMAH&nbsp;
                        <span class="brand-tip">SABLON</span>
                    </span>
                    <span class="brand-mini">RS</span>
                </a>
            </div>
            <div class="flexbox flex-1">
                <!-- START TOP-LEFT TOOLBAR-->
                <ul class="nav navbar-toolbar">
                    <li>
                        <a class="nav-link sidebar-toggler js-sidebar-toggler"><i class="ti-menu"></i></a>
                    </li>
                    <li>
                        @include('partials._new_inbox')
                    </li>
                    <li>
                        @include('partials._clock')
                    </li>
                </ul>
                <!-- END TOP-LEFT TOOLBAR-->
                <!-- START TOP-RIGHT TOOLBAR-->
                <ul class="nav navbar-toolbar">
                    @include('partials._user')
                </ul>
                <!-- END TOP-RIGHT TOOLBAR-->
            </div>
        </header>
        <!-- END HEADER-->
        <!-- START SIDEBAR-->
        <nav class="page-sidebar" id="sidebar">
            <div id="sidebar-collapse">
                <div class="admin-block d-flex">
                    <div>
                        <img src="/template/images/admin-avatar.png" class="rounded-circle" width="45px" />
                    </div>
                    <div class="admin-info">
                        <div class="font-strong">{{ auth()->user()->name }}</div> <small> Super Admin</small></div>
                </div>
                <ul class="side-menu metismenu">
                    @include('partials._menu')
                </ul>
            </div>
        </nav>
        <!-- END SIDEBAR-->
        <div class="content-wrapper">
            <!-- START PAGE CONTENT-->
            @if(isset($title) && isset($sub_title))
            <div class="page-heading">
                <h1 class="page-title">{{ $title }}</h1>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="index.html"><i class="la la-home font-20"></i></a>
                    </li>
                    <li class="breadcrumb-item">{{ $sub_title }}</li>
                </ol>
            </div>
            @endif
            <div class="page-content fade-in-up">
                @yield('content')
            </div>
            <!-- END PAGE CONTENT-->
            <footer class="page-footer">
                <div class="font-13">{{ date('Y') }} © <b>Genius</b> - All rights reserved.</div>
                <a class="px-4" href="#">Version 0.0.01</a>
                <div class="to-top"><i class="fa fa-angle-double-up"></i></div>
            </footer>
        </div>
    </div>

    <!-- BEGIN PAGA BACKDROPS-->
    <div class="sidenav-backdrop backdrop"></div>
    <div class="preloader-backdrop">
        <div class="page-preloader">Loading</div>
    </div>
    <!-- END PAGA BACKDROPS-->
    <audio id="myAudio">
        <source src="/audio/notif.ogg" type="audio/ogg">
        <source src="/audio/notif.mp3" type="audio/mpeg">
    </audio>
    <script>
        var audio = new Audio('/audio/notif.mp3');
        function autoReply(){
                $.ajax({
                    type: "GET",
                    url: "/message/auto-reply",
                    async: false
                });
            }
        function checkInbox(){
            $.ajax({
                type: "GET",
                url: "/message/check-inbox",
                async: false,
                success:function(data) {
                    if(data[0].length){
                        audio.play();
                        document.getElementById('inbox-count').innerHTML = data[0].length;
                    }
                }
            });
        }
        setInterval(function(){ autoReply() }, 1000);
        setInterval(function(){ checkInbox() }, 1500);
    </script>
</body>

</html>
