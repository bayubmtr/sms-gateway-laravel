@extends('layouts.sablon')
@section('content')
<div class="ibox">
    <div class="ibox-head">
        <div class="ibox-title">
            {{ $title }}
        </div>
    </div>

    <div class="ibox-body">
        @include('partials._info')
        <table class="table table-striped table-bordered table-hover" id="table" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th width="10">No</th>
                    <th width="150">Nama Jasa</th>
                    <th width="150">Biaya Jasa</th>
                    <th width="80">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($data as $d)
                <tr>
                    <td>{{ $loop->iteration}}</td>
                    <td>{{ $d->nama }}</td>
                    <td>{{ $d->biaya }}</td>
                    <td align="center">
                        <form id="f_{{$d->id}}" action="{{route('jasa.destroy',$d->id)}}" method="post" style="display:inline">
                        @method('DELETE')
                        @csrf
                        <button onclick="return confirm('Yakin Ingin Hapus Jasa Ini?')"  class="btn  btn-danger" type="submit" value="Delete"><i class="fa fa-trash-o"></i></button>
                        </form>
                        <a href="{{ route('jasa.edit', $d->id) }}" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
<script>
$(document).ready(function(){
        $("#table_length").append('<a  href="{{ route('jasa.create') }}"> <button type="button" class="btn btn-outline-primary ml-3">Tambah</button></a>');
    });
</script>
@endsection