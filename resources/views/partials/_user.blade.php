@if(auth()->user())
<li class="dropdown dropdown-user">
    <a class="nav-link dropdown-toggle link" data-toggle="dropdown">
        <img src="/template/images/admin-avatar.png" alt="">
        <span></span>{{ auth()->user()->name }}<i class="fa fa-angle-down m-l-5"></i></a>
    <ul class="dropdown-menu dropdown-menu-right">
        <a class="dropdown-item" href="{{ route('profile.index') }}"><i class="fa fa-user"></i>Profile</a>
        {{-- <li class="dropdown-divider"></li> --}}
        <a class="dropdown-item" id="logout-btn" href="#"><i class="fa fa-power-off"></i>Logout</a>
    </ul>
</li>
<form id="logout" action="{{route('logout')}}" method="post" style="display:inline">
@csrf
</form>
<script>
var form = document.getElementById("logout");

document.getElementById("logout-btn").addEventListener("click", function () {
  form.submit();
});
</script>
@endif
