<li class="{{ Request::is('/') ? 'active' : '' }}">
    <a href="/"><i class="sidebar-item-icon fa fa-th-large"></i>
        <span class="nav-label">Dashboard</span>
    </a>
</li>
<li class="heading">MENU</li>
<li class="{{ Request::is('message/outbox/create') ? 'active' : '' }}">
    <a href="{{ route('outbox.create') }}"><i class="sidebar-item-icon fa fa-pencil-square-o"></i>
        <span class="nav-label">Tulis Pesan</span>
    </a>
</li>
<li class="{{ Request::is('pesanan*') ? 'active' : '' }}">
    <a href="javascript:;"><i class="sidebar-item-icon fa fa-exchange"></i>
        <span class="nav-label">Transaksi Sablon</span><i class="fa fa-angle-left arrow"></i></a>
    <ul class="nav-2-level collapse">
        <li><a href="{{ route('pesanan.create') }}">Buat Transaksi Baru</a></li>
        <li><a href="{{ route('pesanan.index', ['status' => 'diproses']) }}">Dalam Proses</a></li>
        <li><a href="{{ route('pesanan.index', ['status' => 'selesai']) }}">Transaksi Selesai</a></li>
    </ul>
</li>
<li class="{{ Request::is('message*') ? 'active' : '' }}">
    <a href="javascript:;"><i class="sidebar-item-icon fa fa-envelope-o"></i>
        <span class="nav-label">Folder Pesan</span><i class="fa fa-angle-left arrow"></i></a>
    <ul class="nav-2-level collapse {{ Request::is('message*') ? 'in' : '' }}">
        <li><a href="{{ route('inbox.index') }}">Kotak Masuk</a></li>
        <li><a href="{{ route('outbox.index') }}">Kotak Keluar</a></li>
        <li><a href="{{ route('delivered.index') }}">Pesan Terkirim</a></li>
        <li><a href="{{ route('broadcast.create') }}">Pesan Siaran</a></li>
        <li><a href="{{ route('schedule.index') }}">Pesan Terjadwal</a></li>
    </ul>
</li>
<li class="{{ Request::is('contact*', 'group*') ? 'active' : '' }}">
    <a href="javascript:;"><i class="sidebar-item-icon fa fa-book"></i>
        <span class="nav-label">Kontak</span><i class="fa fa-angle-left arrow"></i></a>
    <ul class="nav-2-level collapse">
        <li><a href="{{ route('contact.index') }}">Daftar Kontak</a></li>
        <li><a href="{{ route('group.index') }}">Daftar Group</a></li>
    </ul>
</li>
<li class="{{ Request::is('profile*', 'jasa*', 'bahan*') ? 'active' : '' }}">
    <a href="javascript:;"><i class="sidebar-item-icon fa fa-gear"></i>
        <span class="nav-label">Konfigurasi</span><i class="fa fa-angle-left arrow"></i></a>
    <ul class="nav-2-level collapse">
        <li><a href="{{ route('profile.index') }}">Profile</a></li>
        <li><a href="{{ route('jasa.index') }}">Jasa</a></li>
        <li><a href="{{ route('bahan.index') }}">Bahan</a></li>
    </ul>
</li>

<!-- Modal -->