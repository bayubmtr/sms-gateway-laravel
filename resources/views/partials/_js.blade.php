
<script src="{{ asset('template/vendors/jquery/jquery.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('template/vendors/popper.js/popper.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('template/vendors/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('template/vendors/metisMenu/metisMenu.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('template/vendors/jquery-slimscroll/jquery.slimscroll.min.js') }}" type="text/javascript"></script>

<script src="{{ asset('template/vendors/DataTables/datatables.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('template/vendors/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}" type="text/javascript"></script>
<script src="{{ asset('template/vendors/chart.js/Chart.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('template/vendors/select2/js/select2.min.js') }}" type="text/javascript"></script>

<script src="{{ asset('template/js/app.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('template/js/scripts/dashboard_1_demo.js') }}" type="text/javascript"></script>
