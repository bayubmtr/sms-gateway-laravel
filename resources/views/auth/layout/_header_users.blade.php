
<link href="{{ asset('template/vendors/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" />
<link href="{{ asset('template/vendors/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" />
<link href="{{ asset('template/vendors/themify-icons/css/themify-icons.css') }}" rel="stylesheet" />
<link rel="shortcut icon" type="image/x-icon" href="{{ asset('template/images/weaboeneeds.png') }}" />

<link href="{{ asset('template/vendors/jvectormap/jquery-jvectormap-2.0.3.css') }}" rel="stylesheet" />
<link href="{{ asset('template/vendors/DataTables/datatables.min.css') }}" rel="stylesheet" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote-bs4.css" rel="stylesheet">
<link href="{{ asset('template/vendors/select2/css/select2.min.css') }}" rel="stylesheet" />
<link href="{{ asset('template/vendors/bootstrap-datepicker/css/bootstrap-datepicker.min.css')}}" rel="stylesheet" />

<link href="{{ asset('template/vendors/bs4-tags-input/tagsinput.css') }}" rel="stylesheet" />
<link href="{{ asset('template/css/main.min.css') }}" rel="stylesheet" />
<link href="{{ asset('template/css/custom.css') }}" rel="stylesheet" />
