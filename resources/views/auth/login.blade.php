<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width initial-scale=1.0">
    <title>Admin | Login</title>
    @include('auth.layout._header_users')
    @include('auth.layout._footer_users')
    <!-- PAGE LEVEL STYLES-->
    <link href="{{ asset('template/css/pages/auth-light.css') }}" rel="stylesheet" />
</head>

<body class="bg-silver-300">
    <div class="content">
        <div class="brand">
            <br/>
        </div>

        {{ Form::open(['id' => 'login-form', 'route' => ['login'], 'method' => 'POST']) }}
            @csrf

            <center class="mt-3 mb-5">
                <h3>LOGIN</h3>
                <h2><strong>RUMAH SABLON</strong></h2>

            </center>

            @if ($errors->any())
                <div class="alert alert-danger alert-block">
                    <button type="button" class="close" data-dismiss="alert">x</button>
                    @foreach ($errors->all() as $error)
                            <ul>
                                <li><strong>{{ $error }}</strong></li>
                            </ul>
                    @endforeach
                </div>
            @endif

            <div class="form-group">
                {{ Form::label('username', 'Email', ['class' => 'font-bold']) }}
                <div class="input-group-icon left">
                    <div class="input-icon"><i class="fa fa-envelope text-info"></i></div>
                    {{ Form::text('email', null, ['class' => 'form-control', 'id' => 'username', 'placeholder' => 'Username or Email']) }}
                </div>
            </div>
            <div class="form-group">
                {{ Form::label('password', 'Password', ['class' => 'font-bold']) }}
                <div class="input-group-icon left">
                    <div class="input-icon"><i class="fa fa-envelope text-info"></i></div>
                    {{ Form::password('password', ['class' => 'form-control', 'id' => 'password', 'placeholder' => 'Password']) }}
                </div>
            </div>
            <div class="form-group d-flex justify-content-between">
                <a href="{{ route('register') }}">Belum Punya Akun?</a>
            </div>
            <div class="form-group">
                {{ Form::button('<i class="fa fa-sign-in"></i> Login', ['type' => 'submit', 'class' => 'btn btn-primary btn-block']) }}
            </div>
        {{ Form::close() }}
    </div>
    <!-- BEGIN PAGA BACKDROPS-->
    <div class="sidenav-backdrop backdrop"></div>
    <div class="preloader-backdrop">
        <div class="page-preloader">Loading</div>
    </div>
    <!-- END PAGA BACKDROPS-->
    <script src="{{ asset('template/vendors/jquery-validation/dist/jquery.validate.min.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
        $(function() {
            $('#login-form').validate({
                errorClass: "help-block",
                rules: {
                    email: {
                        required: true,
                        email: true
                    },
                    password: {
                        required: true
                    }
                },
                highlight: function(e) {
                    $(e).closest(".form-group").addClass("has-error")
                },
                unhighlight: function(e) {
                    $(e).closest(".form-group").removeClass("has-error")
                },
            });
        });
    </script>
</body>

</html>
