@extends('layouts.sablon')
@section('content')
<link href="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/css/select2.min.css" rel="stylesheet" />
<div class="row">
    <div class="col-6">
        <div class="ibox">
            <div class="ibox-head">
                <div class="ibox-title">
                {{ isset($title) ? $title : '' }}
                </div>
            </div>
            <div class="ibox-body">
            @include('partials._info')
                <form id="form_tulis_pesan" action="{{ route('contact.update', $data->ID) }}" method="post">
                    @csrf
                    @method('PATCH')
                    <div class="form-group">
                        <label for="">Nama Pelanggan</label>
                        <div class="input-group">
                            <input type="text" class="form-control" name="Name" value="{{ $data->Name }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="">Grup</label>
                        <div class="input-group-icon left">
                            <select id="kontak" class="js-example-basic-single form-control" name="GroupID">
                                <option value="0">-- Pilih Grup --</option>
                                @foreach($group as $k)
                                <option {{ $data->GroupID == $k->GroupID ? 'selected' : '' }} value="{{$k->GroupID}}">{{$k->NameGroup}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="">Nomor HP</label>
                        <div class="input-group">
                            <input type="text" class="form-control" name="Number" value="{{ $data->Number }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-danger" onclick="window.history.go(-1)" type="button"><i class="fa fa-arrow-left"></i>
                            Batal</button>
                            <button class="btn btn-primary" type="submit"><i class="fa fa-floppy-o"></i> Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
$(document).ready(function() {
    $('.js-example-basic-single').select2();
});
</script>
@endsection