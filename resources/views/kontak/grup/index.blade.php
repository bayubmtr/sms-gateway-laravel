@extends('layouts.sablon')
@section('content')
<div class="ibox">
    <div class="ibox-head">
        <div class="ibox-title">
            {{ $title }}
        </div>
    </div>

    <div class="ibox-body">
        @include('partials._info')
        <table class="table table-striped table-bordered table-hover" id="table" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th width="10">No</th>
                    <th width="150">Nama Grup</th>
                    <th width="80">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($data as $d)
                <tr>
                    <td>{{ $loop->iteration}}</td>
                    <td>{{ $d->NameGroup }}</td>
                    <td align="center">
                        <form id="f_{{$d->GroupID}}" action="{{route('group.destroy',$d->GroupID)}}" method="post" style="display:inline">
                        @method('DELETE')
                        @csrf
                        <button onclick="return confirm('Yakin Ingin Hapus Group Ini?')"  class="btn  btn-danger" type="submit" value="Delete"><i class="fa fa-trash-o"></i></button>
                        </form>
                        <a href="{{ route('group.edit', $d->GroupID) }}" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
<script>
$(document).ready(function(){
        $("#table_length").append('<a  href="{{ route('group.create') }}"> <button type="button" class="btn btn-outline-primary ml-3">Tambah</button></a>');
    });
</script>
@endsection