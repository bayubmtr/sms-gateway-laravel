<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();
Route::group(['middleware' => 'auth'], function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::group(['prefix' => 'message', 'namespace' => 'Message'], function () {
        Route::resource('outbox', 'OutboxController');
        Route::resource('delivered', 'DeliveredController');
        Route::resource('inbox', 'InboxController');
        Route::get('check-inbox', 'InboxController@checkInbox')->name('inbox.check');
        Route::resource('broadcast', 'BroadCastController');
        Route::resource('schedule', 'ScheduleController');
        Route::get('check-schedule', 'ScheduleController@checkSchedule')->name('schedule.check');
        Route::resource('auto-reply', 'AutoReplyController');
    });
    Route::group(['namespace' => 'Pesanan'], function () {
        Route::resource('pesanan', 'PesananController');
        Route::get('pesanan-export', 'PesananController@export')->name('pesanan.export');
    });
    Route::group(['namespace' => 'Contact'], function () {
        Route::resource('contact', 'ContactController');
        Route::resource('group', 'GroupController');
    });
    Route::group(['namespace' => 'Bahan'], function () {
        Route::resource('bahan', 'BahanController');
    });
    Route::group(['namespace' => 'Jasa'], function () {
        Route::resource('jasa', 'JasaController');
    });
    Route::group(['namespace' => 'Profile'], function () {
        Route::resource('profile', 'ProfileController');
    });
});
